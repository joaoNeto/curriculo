<?php 

	date_default_timezone_set('America/Recife');
	include "conexao.php";

	$ipaddress = '';
	     
	if ($_SERVER['HTTP_CLIENT_IP'])
	    $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
	else if($_SERVER['HTTP_X_FORWARDED_FOR'])
	    $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
	else if($_SERVER['HTTP_X_FORWARDED'])
	    $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
	else if($_SERVER['HTTP_FORWARDED_FOR'])
	    $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
	else if($_SERVER['HTTP_FORWARDED'])
	    $ipaddress = $_SERVER['HTTP_FORWARDED'];
	else if($_SERVER['REMOTE_ADDR'])
	    $ipaddress = $_SERVER['REMOTE_ADDR'];
	else
	    $ipaddress = 'UNKNOWN';

	$horario = date("d/m/y - h:m:s");

	$sql  = "INSERT INTO  informacoes_cliente(ip_cliente,horario) VALUES (:ip_cliente,:horario)";
	$stmt = ConnBD::prepare($sql);
	$stmt-> bindParam(':ip_cliente', $ipaddress);
	$stmt-> bindParam(':horario', $horario);
	$stmt->execute();


?>