<!DOCTYPE html>
<html>
<head>
	<title>Este é o João Neto</title>
	<meta charset="utf-8">
	<link rel="stylesheet" type="text/css" href="estilo.css">
	<script type="text/javascript" src="javascript.js"></script>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link href="https://fonts.googleapis.com/css?family=Josefin+Sans" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Poiret+One" rel="stylesheet">

</head>
<body>

<div id="quem-sou-eu">
	
	<div id="identificacao">
		<center>
			
			<div id="meu-nome">
				<font id="font1">Olá eu sou</font> 
				<font id="font2" onmouseover="ocarashow()" onmouseout="ocaraleave()">João Neto</font> 
				<font id="font3"> Esse ai é o cara! </font>
			</div>
		
			<table>
				<tr><td><a href="#linhadotempo" ><font id="font5"><b>Minha vida</b></font></a></td></tr>
				<tr><td></td></tr>
				<tr><td><a href="#contato"><font id="font5"><b>Fale comigo</b></font></a></td></tr>
			</table>

		</center>
	</div>
	
	<div id="qualidades">

		<div id="habilidades">
			
			<table border="0" id="tabela-habilidades">
				<tr>	<td><font id="font6">HTML</font></td>	<td></td>	<td><font id="font6">JQUERY</font></td>	</tr>
				<tr>	<td></td>	<td><font id="font6">MYSQL</font></td>	<td><font id="font6">CSS</font></td>	</tr>
				<tr>	<td><font id="font6">PHP</font></td>	<td></td>	<td><font id="font6">JAVASCRIPT</font></td>	</tr>
			</table>
				
		</div>

		<div id="formacoes">
			<table border="0" style="float:right;">
				<tr><td><font id="font4">Desenvolvedor front-end e back-end</font></td></tr>
				<tr><td><font id="font4">Técnico em informática para internet</font></td></tr>
				<tr><td><font id="font4">Graduando em ciencias da computação</font></td></tr>
			</table>
		</div>
		<img src="imagens/flecha2.png" id="flecha1">

		<div id="saber-mais">
			<font id="texto-saber-mais">Deseja saber mais sobre mim???</font>

			<a href="#linhadotempo" ><input type="submit" name="Clique aqui" value="Clique aqui!!" id="botao-saber-mais"></a>

		</div>

	</div>

	<div id="minha-imagem">
		<div id="habilidades">

			<table border="0" id="tabela-habilidades">
				<tr>	<td><font id="font6">Wordpress</font></td>	<td></td>	<td></td>	</tr>
				<tr>	<td><font id="font6">GIT</font></td>	<td><font id="font6">Joomla</font></td>	<td></td>	</tr>
				<tr>	<td><font id="font6">CorelDRAW</font></td>	<td></td>	<td></td>	</tr>
			</table>

		</div>
	</div>
	
</div>

<div id="linhadotempo">

	<div id="ceu">
	
		<div id="divisaonuvem"><img src="imagens/nuvem1.png" class="nuvem" id="nuvem1"></div>
		<div id="divisaonuvem"><img src="imagens/nuvem2.png" class="nuvem" id="nuvem2"></div>
		<div id="divisaonuvem"><img src="imagens/nuvem5.png" class="nuvem" id="nuvem3"></div>
		<div id="divisaonuvem"><img src="imagens/nuvem4.png" class="nuvem" id="nuvem4"></div>
		<div id="divisaonuvem"><img src="imagens/nuvem3.png" class="nuvem" id="nuvem5"></div>

	</div>

	<font id="font7">Está é a minha vida...</font>
	
	<iframe src="timeline/horizontal-timeline/index.html" id="iframe-linha-do-tempo" scrolling="no"></iframe>

	 <div id="rodapelinhadotempo">
	 	<img src="imagens/neto.png" id="eu-habbo" onmouseout="bracobaixo()" onmouseover="bracocima()">
	 	<img src="imagens/netohover.png" id="eu-habbo2" onmouseout="bracobaixo()" onmouseover="bracocima()">
	 </div>

</div>

<div id="contato">
	<font id="font7" style="margin-top:50px;width:70%;">Entre em contato comigo...</font>
	<div id="redes-sociais"> 
		<font id="font11" style="color:white;">Meus contatos</font> 
		<table id="tabela-contato-rodape">
			<tr><td><b>Email:</b> eujoaonetoti@outlook.com </td></tr>
			<tr><td><b>Telefone:</b> (81) 9 9796 - 6952 </td></tr>
			<tr><td><b>Endereço:</b> Ed.Pascareta, n° 105. Av. João de Barros, 1890 - Encruzilhada, Recife - PE. </td></tr>
		</table>

		<center>
			<font style="color:white;	font-family: 'Poiret One', cursive;font-size:20px;color:white;cursor:default;">Baixe meu currículo em .docx </font><br>
			 <a href="CV Joao Neto.docx" style="text-decoration:none;">
			 	<input type="submit" name="Meu Curriculo" value="Meu Currículo.docx" id="input-submit2">
			 </a>
		</center>
	</div>
	<div id="formulario">
		<font id="font11">Deixe seu recado</font> 
		<form id="form-formulario" action="enviar_Email.php" method="POST">
			<input 		type="name" name="nome" 	placeholder="Digite seu nome"		id="input" />
			<input 		type="name" name="email" 	placeholder="Digite seu email"		id="input" />
			<textarea 				name="mensagem"	placeholder="Digite sua mensagem"	id="input" style="width:150%;height:35%" ></textarea>
			<input 		type="submit" name="Enviar" value="Enviar" 					id="input-submit" />
		</form>
	</div>
	<div id="rodape-contato">		
		<center>
			<font id="font10">
				Desenvolvido por
				<br>
				<b>João Neto</b>
			</font>
		</center>
	</div>

</div>
<script src="http://code.jquery.com/jquery-1.11.3.min.js"></script>
<script type="text/javascript">
	
	var i = 0;

	var nuvem1 = function () {
		i++;		
		if(i%2 == 0){
			document.getElementById("nuvem1").style="margin-left:10px;transition:1s";			
		}else{
			document.getElementById("nuvem1").style="margin-left:0px;transition:1s";			
		}
	    setTimeout(nuvem1,5000);
	}

	var nuvem2 = function () {
		i++;		
		if(i%2 == 0){
			document.getElementById("nuvem4").style="margin-left:10px;transition:1s";			
		}else{
			document.getElementById("nuvem4").style="margin-left:0px;transition:1.4s";			
		}
	    setTimeout(nuvem2,2000);
	}
	var nuvem3 = function () {
		i++;		
		if(i%2 == 0){
			document.getElementById("nuvem3").style="margin-left:10px;transition:1.5s";			
		}else{
			document.getElementById("nuvem3").style="margin-left:0px;transition:1.5s";			
		}
	    setTimeout(nuvem3,1000);
	}

	nuvem1();	
	nuvem2();	
	nuvem3();	




	$(function() {
	  $('a[href*=#]:not([href=#])').click(function() {
	    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
	      var target = $(this.hash);
	      target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
	      if (target.length) {
	        $('html,body').animate({
	          scrollTop: target.offset().top
	        }, 1000);
	        return false;
	      }
	    }
	  });
	});

	

</script>

</body>
</html>

<?php 

	

	include "informacao_cliente.php";

	if($_GET['m'] == 'y'){
		echo "
			<script type='text/javascript'>
				alert('Mensagem enviada com sucesso!');
			</script>
		";
	}

?>
