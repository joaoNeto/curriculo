<?php

error_reporting(0);

class ConnBD{

	private static $instance;
	
	public static function getInstance(){
				
		if(!isset(self::$instance)){
			
			try{
				
				self::$instance = new PDO('mysql:host=mysql.hostinger.com.br;dbname=u713371702_banco','u713371702_root','123asd123asd');
				self::$instance->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
				self::$instance->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ);
				self::$instance->exec("SET CHARACTER SET utf8");
			}catch(PDOException $e){
				
				echo $e->getMessage();
				
			}
			
		}
		
		return self::$instance;
	}
	
	public static function prepare($sql){
		
		return self::getInstance()->prepare($sql);
		
	}
	
}

?>